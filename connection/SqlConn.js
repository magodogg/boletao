'use strict';
var tp = require('tedious-promises');
var dbConfig = require('./config.json');
var type = require('tedious').TYPES;
tp.setConnectionConfig(dbConfig); // global scope

var sql = {}

sql.get_boleto = function (id_pagamento) {
    return tp.sql(`select id_registro_boleto,
     id_pedido, id_pagamento,
     nome_projeto, nome, Sobrenome, valor, 
     convert(varchar,data_emissao,103) as data_emissao
     from tb_registro_boleto
     where id_pagamento = @id_pagamento`).parameter("id_pagamento", type.Int, id_pagamento)
     .execute()
}

sql.get_deleta = function (id_pagamento) {
    return tp.sql(`delete from tb_registro_boleto
     where id_pagamento = @id_pagamento`).parameter("id_pagamento", type.Int, id_pagamento)
     .execute()
}

module.exports = sql