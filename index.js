var express = require("express");
var app = express();
var body_parser = require("body-parser");
var sql = require("./connection/SqlConn");

app.use(body_parser.urlencoded({
    extended: false
}))
app.use(body_parser.json())
app.set("view engine", 'ejs');
app.use(express.static("public"));

var porta = 7412

app.get("/", function(req,res){
    res.render("home");
});

app.post("/manobra_boleto", function (req, res) {
    var id_pagamento = req.body.id_pagamento;
    sql.get_boleto(id_pagamento).then(function(results){
        
        res.send(results);
    }).fail(function(err){
        console.log(err);
        res.send("Error");
    })    
});

app.post("/deleta_boleto", function (req, res) {
    var id_pagamento = req.body.id_pagamento;
    sql.get_deleta(id_pagamento).then(function(results){
        
        res.send(results);
    }).fail(function(err){
        console.log(err);
        res.send({
            status: "Erro",
            erro: err
        });
    })    
});




app.listen(porta, function (err) {
    if (err) console.log(err);
    console.log("Online on Port = " + porta);
});